/*
Geolocation project using google map API

File-name : script.js
Description: A geolocation project to get the nearby places.
Files: index.html and script.js

Author: Karan Kanwal
Contact: karan.kanwal@mountblue.io
*/



let lati;
let longi;
let latlang;
let map;
let x = document.getElementById('location');
let googleMap = document.getElementById('googleMap');
let places  = [
    'movie_theater',
    'bus_station',
    'shoping_mall',
    'taxi_stand',
    'hospital'
  ];

let markers = {
    movie_theater: "https://img.icons8.com/material/24/000000/movie.png",
    bus_station :"https://img.icons8.com/material/24/000000/bus--v1.png" ,
    shoping_mall: "https://img.icons8.com/material/24/000000/shopping-cart.png",
    taxi_stand: "https://img.icons8.com/material/24/000000/taxi.png",
    hospital:"https://img.icons8.com/material/24/000000/hospital.png"
}

let infoWindow;;
  

function getLocation(){
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }else{
        x.innerHTML = "No support for geolocation";
    }
}

function showPosition(position){
    lati = position.coords.latitude;
    longi = position.coords.longitude;

    x.innerHTML = "Latitude = "+ lati + "</br> Longitude = "+ longi;
    console.log(lati);
    console.log(longi);
    latlang = new google.maps.LatLng(lati,longi);

    var mymap = {
        center: latlang,
        zoom: 15,
    };
    map = new google.maps.Map(googleMap, mymap);

    var marker = new google.maps.Marker({position: latlang});
    marker.setMap(map);

    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    
    for(let place of places){
        service.nearbySearch({
            location: latlang,
            radius: 1000,
            type: [place]
            }, callback);
    }
        
}

function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

      
    function createMarker(place) {
        console.log(place);
        var placeLoc = place.geometry.location;
            marker = new google.maps.Marker({
            icon: markers[place.types[0]],
            map: map,
            position: place.geometry.location,
    });
     
      

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}


function showError(error) {
    switch(error.code) {
      case error.PERMISSION_DENIED:
        x.innerHTML = "User denied the request for Geolocation."
        break;
      case error.POSITION_UNAVAILABLE:
        x.innerHTML = "Location information is unavailable."
        break;
      case error.TIMEOUT:
        x.innerHTML = "The request to get user location timed out."
        break;
      case error.UNKNOWN_ERROR:
        x.innerHTML = "An unknown error occurred."
        break;
    }
}
  
